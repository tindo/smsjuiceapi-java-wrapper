package com.smsjuice.api.wrapper.domain.model;

import static org.junit.Assert.*;

import org.junit.Test;

import com.smsjuice.api.wrapper.domain.model.Contact;
import com.smsjuice.api.wrapper.exception.ContactException;

public class ContactTest {

	@Test
	public void testToAddress(){
		testAddress("+447833686345");
		testAddress("00447833686345");
		testAddress("447833686345");
		testAddress("686345");
		testAddress("447833686345222");

	}
	
	@Test
	public void testThrownMessageException(){
		assertTrue(testException("+4478336863452345"));
		assertTrue(testException("447833686e4s"));
		assertTrue(testException("45511"));
		assertTrue(testException("aaaaaa"));
		assertTrue(testException("07900683356"));
	}
	
	
	private void testAddress(String input){
		try {
			new Contact(input);
		} catch (ContactException e) {
			fail(input + " failed the test.");
		}
	}
	
	
	private boolean testException(String input){
		boolean thrown = false;
		try {
			new Contact(input);
		} catch (ContactException e) {
			thrown = true;
		}
		return thrown;
	}
	
}
