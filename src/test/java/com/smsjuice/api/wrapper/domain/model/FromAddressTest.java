package com.smsjuice.api.wrapper.domain.model;

import static org.junit.Assert.*;

import org.junit.Test;

import com.smsjuice.api.wrapper.domain.model.FromAddress;
import com.smsjuice.api.wrapper.exception.MessageException;

public class FromAddressTest {

	
	@Test
	public void testFromAddress(){
		
		assertTrue(testAddress("131541351351351"));
		assertTrue(testAddress("00423654655601547"));
		assertTrue(testAddress("+423654655601547"));
		assertTrue(testAddress("447900385584"));
		assertTrue(testAddress("11character"));
		assertTrue(testAddress("324"));
		assertTrue(testAddress("123456"));
		
		
		assertFalse(testAddress("1131541351351351"));
		assertFalse(testAddress("alphanumeric"));
		assertFalse(testAddress("al-pha++"));
		assertFalse(testAddress("4+23654655601547"));
		
		
	}
	
	private boolean testAddress(String input){
		boolean ret = true;
		try {
			new FromAddress(input);
		} catch (MessageException e) {
			System.out.println(e.getMessage());
			ret = false;
		}
		
		return ret;
	}
	
	
}
