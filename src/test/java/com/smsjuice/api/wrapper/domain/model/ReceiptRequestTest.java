package com.smsjuice.api.wrapper.domain.model;

import static org.junit.Assert.*;

import org.junit.Test;

import com.smsjuice.api.wrapper.domain.model.ReceiptRequest;
import com.smsjuice.api.wrapper.exception.MessageException;

public class ReceiptRequestTest {
	
	@Test
	public void testReceiptRequest(){
		//appropriate receipt requests will return true
		assertTrue(setRequestReceipt(0));
		assertTrue(setRequestReceipt(1));
		
		//should return false when exception is thrown to show inappropriate receiptrequest value
		assertFalse(setRequestReceipt(2));
		assertFalse(setRequestReceipt(-1));
	}

	private boolean setRequestReceipt(int i) {
		boolean truereceiptrequest = true;
		try {
			ReceiptRequest.instanceFromValue(i);
		} catch (MessageException e) {
			truereceiptrequest = false;
		}
		return truereceiptrequest;
	}
}
