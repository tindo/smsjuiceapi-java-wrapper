package com.smsjuice.api.wrapper.domain.model;

import static org.junit.Assert.*;

import org.junit.Test;

import com.smsjuice.api.wrapper.exception.RecipientTypeException;

public class RecipientTypeTest {

	
	@Test
	public void testRecipientTypeException(){
		try {
			RecipientType r = RecipientType.instanceFromValue("wrong value");
			fail();
		} catch (RecipientTypeException e) {
			
		}
	}
	
	@Test
	public void testRecipientTypeGroups(){
		try {
			RecipientType r = RecipientType.instanceFromValue("groups");
			r = RecipientType.instanceFromValue("GROUPS");
			assertEquals(RecipientType.GROUPS, r);
			r = RecipientType.instanceFromValue("grOuPs");
			assertEquals(RecipientType.GROUPS, r);
			r = RecipientType.instanceFromValue("grOuPs ");
			assertEquals(RecipientType.GROUPS, r);
		} catch (RecipientTypeException e) {
			fail();
		}
	}
	
	@Test
	public void testRecipientTypeContacts(){
		try {
			RecipientType r = RecipientType.instanceFromValue("Numbers");
			r = RecipientType.instanceFromValue("numbers");
			assertEquals(RecipientType.NUMBERS, r);
			r = RecipientType.instanceFromValue("NUMBERS");
			assertEquals(RecipientType.NUMBERS, r);
			r = RecipientType.instanceFromValue("numbers ");
			assertEquals(RecipientType.NUMBERS, r);
			
		} catch (RecipientTypeException e) {
			e.printStackTrace();
			fail();	
		}
	}
}
