package com.smsjuice.api.wrapper.domain.model;

import static org.junit.Assert.*;

import org.junit.Test;

import com.smsjuice.api.wrapper.domain.model.SMSCharacterEncoding;
import com.smsjuice.api.wrapper.exception.MessageException;

public class SMSCharacterEncodingTest {

	@Test
	public void testSMSEncoding(){
		//appropriate encoding
		assertTrue(getEncoding(0));
		assertTrue(getEncoding(3));
		assertTrue(getEncoding(8));
		
		//inappropriate encoding
		assertFalse(getEncoding(-1));
		assertFalse(getEncoding(9));
	}

	private boolean getEncoding(int en){
		boolean rightencoding = true;
		try {
			SMSCharacterEncoding.instanceFromValue(en);
		} catch (MessageException e) {
			rightencoding = false;
		}
		return rightencoding;
	}
}
