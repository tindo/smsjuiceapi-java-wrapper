package com.smsjuice.api.wrapper.domain.model;

import static org.junit.Assert.*;

import org.junit.Test;

import com.smsjuice.api.wrapper.TextMessagingService;
import com.smsjuice.api.wrapper.TextMessagingServiceFactory;
import com.smsjuice.api.wrapper.domain.model.Message;
import com.smsjuice.api.wrapper.exception.MessageException;
import com.smsjuice.api.wrapper.exception.RecipientTypeException;

public class MessageTest {
	

	private TextMessagingService textMessagingService = TextMessagingServiceFactory.getTestInstance();

	@Test
	public void testNullMessageException(){
		try {
			@SuppressWarnings({ "unused", "deprecation" })
			Message m = textMessagingService.createMessageInstance(null,null,null);
			fail("no exception thrown");
		} catch (MessageException | RecipientTypeException e) {
			//expected
		}
	}
	
	@Test
	public void testMessageDates(){
		assertTrue(testDate("2015-03-30 00:05"));
		
		assertFalse(testDate("2015-03-30 00:05:00"));
		assertFalse(testDate("2015-03-30 00:0"));
		assertFalse(testDate("2015-03-30 00:65"));
		assertFalse(testDate("2015-03-30 74:05"));
		assertFalse(testDate("2015-03-37 00:05"));
		assertFalse(testDate("2015-13-16 00:05"));
		assertFalse(testDate("2015-03-30"));
		assertFalse(testDate(""));
		assertFalse(testDate(null));
	}

	@Test
	public void testExpiryDates(){
		assertTrue(testDate("2015-03-30 00:05"));
		
		assertFalse(testExpiryDate("2015-03-30 00:05:00"));
		assertFalse(testExpiryDate("2015-03-30 00:0"));
		assertFalse(testExpiryDate("2015-03-30 00:65"));
		assertFalse(testExpiryDate("2015-03-30 74:05"));
		assertFalse(testExpiryDate("2015-03-37 00:05"));
		assertFalse(testExpiryDate("2015-13-16 00:05"));
		assertFalse(testExpiryDate("2015-03-30"));
		assertFalse(testExpiryDate(""));
		assertFalse(testExpiryDate(null));
	}
	
	@Test
	public void testMessageContent(){
		assertTrue(testMessage("a"));
		//empty message
		assertFalse(testMessage(""));
		//longer than 765 characters (5 text messages)
		assertFalse(testMessage("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod "
				+ "tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis "
				+ "nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis "
				+ "aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat "
				+ "nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui "
				+ "officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur "
				+ "adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut e"
				+ "nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea co"
				+ "mmodo consequat.Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adi"
				+ "piscing elit, sed do eiusmod"));
		
		assertFalse(testMessage(null));
	}
	
	public void testMessageCreationFromTemplateWithZeroTemplateIdThrowsException(){
		try {
			Message m = new Message("kei", "seikret", RecipientType.NUMBERS, 0);
			fail("Exception must be thrown because of templateId");
		} catch (MessageException e) {
			//expected
		}
	}
	
	public void testMessageCreationFromTemplateThrowsExceptionWhenMessageIsAdded(){
		try {
			Message m = new Message("kei", "seikret", RecipientType.NUMBERS, 1);
			m.setMessage("Hello");
			fail("Exception must be thrown because both message and templateId cannot be set at the same time");
		} catch (MessageException e) {
			//expected
		}
	}
	
	
	private boolean testMessage(String message){
		boolean messagetest = true;
		try {
			if(message==null || message.trim().isEmpty()){
				throw new MessageException("Empty message");
			}
			Message m =textMessagingService.createMessageInstance("kei","seikret",RecipientType.GROUPS,message);
		} catch (MessageException e) {
			//invalid dates
			messagetest = false;
		}
		
		return messagetest;
	}
	
	private boolean testDate(String date){
		boolean datetest = true;
		try {
			Message m =textMessagingService.createMessageInstance("kei","seikret",RecipientType.GROUPS,"hello");
			m.setSendDate(date);
		} catch (MessageException e) {
			//invalid dates
			datetest = false;
		}
		return datetest;
	}

	private boolean testExpiryDate(String date){
		boolean datetest = true;
		try {
			Message m =textMessagingService.createMessageInstance("kei","seikret",RecipientType.GROUPS,"hello");
			m.setExpiryDate(date);
		} catch (MessageException e) {
			//invalid dates
			datetest = false;
		}
		return datetest;
	}
	
}
