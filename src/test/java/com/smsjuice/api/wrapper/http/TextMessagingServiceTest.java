package com.smsjuice.api.wrapper.http;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.ws.rs.core.Response;

import org.junit.Rule;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.smsjuice.api.wrapper.TextMessagingService;
import com.smsjuice.api.wrapper.TextMessagingServiceFactory;
import com.smsjuice.api.wrapper.domain.model.Contact;
import com.smsjuice.api.wrapper.domain.model.ContactGroup;
import com.smsjuice.api.wrapper.domain.model.Message;
import com.smsjuice.api.wrapper.domain.model.NamedContact;
import com.smsjuice.api.wrapper.domain.model.RecipientType;
import com.smsjuice.api.wrapper.domain.model.StringUtils;
import com.smsjuice.api.wrapper.exception.ContactException;
import com.smsjuice.api.wrapper.exception.MessageException;
import com.smsjuice.api.wrapper.exception.RecipientTypeException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

public class TextMessagingServiceTest extends AbstractServiceTest{
	
	protected static final ObjectMapper MAPPER = new ObjectMapper();
	
	private TextMessagingService textMessagingService = TextMessagingServiceFactory.getTestInstance();
	
	@Test
	public void testSendMessageToNumbersIsSuccessful(){
		
	    stubFor(post(urlEqualTo("/send"))
	            .withHeader("Accept", equalTo("application/json"))
	            .willReturn(aResponse()
	                .withStatus(200)));
		
		try {
			ArrayList<String> recipients = new ArrayList<String>();
			recipients.add("118118");
			recipients.add("abcdd");
			recipients.add("07936855862");
			recipients.add("077861265f5");
			Message m = textMessagingService.createMessageInstance("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe", "numbers");
			m.setFrom("shabba").addRecipient("447900385586").addRecipient("263790028495").addRecipientList(recipients).setMessage("What time are you coming home?");
		} catch (MessageException e) {
			fail("unexpected");
			e.printStackTrace();
		} catch (ContactException e) {
			fail("unexpected");
			e.printStackTrace();
		} catch (RecipientTypeException e) {
			e.printStackTrace();
			fail("unexpected");
			e.printStackTrace();
		}
	}
	
	
	
	
	@Test
	public void testSendMessageWithNewMessageConstructorSuccessful(){
		String expectedJsonBody = "{\"encoding\":0,\"receipt\":1,\"key\":\"gg0hs68y\",\"secret\":\"e5087bac141897ab37af89bae8d5dc1447ab4efe\","
				+ "\"from\":\"Nyaradzo\",\"message\":\"Hello World\",\"schedule\":null,\"expiry\":null,"
				+ "\"to\":[\"447900385586\",\"263790028495\",\"277790839483\",\"263778394993\"],\"recipientType\":\"numbers\",\"templateId\":0}";
	    stubFor(post(urlEqualTo("/send"))
	            .withHeader("Accept", equalTo("application/json"))
	            .withRequestBody(containing(expectedJsonBody))
	            .willReturn(aResponse()
	                .withStatus(200)));
		try {
			ArrayList<String> recipients = new ArrayList<String>();
			recipients.add("277790839483");
			recipients.add("263778394993");
			Message m = textMessagingService.createMessageInstance("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe",RecipientType.NUMBERS,"Hello World");
			m.setFrom("Nyaradzo").addRecipient("447900385586").addRecipient("263790028495").addRecipientList(recipients);
			Response r = textMessagingService.sendMessage(m);
			assertEquals(200, r.getStatus());
		} catch (MessageException e) {
			fail("unexpected");
			e.printStackTrace();
		} catch (ContactException e) {
			fail("unexpected");
			e.printStackTrace();
		} catch (IOException e) {e.printStackTrace();
			fail("unexpected");
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testSendMessageWithNewTemplateConstructorSuccessful(){
		String expectedJsonBody = "{\"encoding\":0,\"receipt\":1,\"key\":\"gg0hs68y\",\"secret\":\"e5087bac141897ab37af89bae8d5dc1447ab4efe\","
				+ "\"from\":\"shabba\",\"message\":null,\"schedule\":null,\"expiry\":null,\"to\":[\"447900385586\",\"263790028495\",\"118118\"],"
				+ "\"recipientType\":\"numbers\",\"templateId\":58}";
	    stubFor(post(urlEqualTo("/send"))
	            .withHeader("Accept", equalTo("application/json"))
	            .withRequestBody(containing(expectedJsonBody))
	            .willReturn(aResponse()
	                .withStatus(200)));
		try {
			ArrayList<String> recipients = new ArrayList<String>();
			recipients.add("118118");
			recipients.add("abcdd");
			recipients.add("07936855862");
			recipients.add("077861265f5");
			Message m = textMessagingService.createMessageInstance("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe",RecipientType.NUMBERS,58);
			m.setFrom("shabba").addRecipient("447900385586").addRecipient("263790028495").addRecipientList(recipients);
			assertEquals(200,textMessagingService.sendMessage(m).getStatus());
		} catch (MessageException e) {
			fail("unexpected");
			e.printStackTrace();
		} catch (ContactException e) {
			fail("unexpected");
			e.printStackTrace();
		} catch (IOException e) {
			fail("unexpected");
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testSendMessageToGroupsIsSuccessful(){
		String expectedJsonBody = "{\"encoding\":0,\"receipt\":1,\"key\":\"gg0hs68y\",\"secret\":\"e5087bac141897ab37af89bae8d5dc1447ab4efe\","
				+ "\"from\":\"shabba\",\"message\":\"What time are you coming home?\",\"schedule\":null,\"expiry\":null,\"to\":[\"JandD\"],"
				+ "\"recipientType\":\"groups\",\"templateId\":0}";
	    stubFor(post(urlEqualTo("/send"))
	            .withHeader("Accept", equalTo("application/json"))
	            .withRequestBody(containing(expectedJsonBody))
	            .willReturn(aResponse()
	                .withStatus(200)));
		try {
			ArrayList<String> recipients = new ArrayList<String>();
			recipients.add("JandD");
			Message m = textMessagingService.createMessageInstance("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe", "groups");
			m.setFrom("shabba").addRecipientList(recipients).setMessage("What time are you coming home?");
			assertEquals(200,textMessagingService.sendMessage(m).getStatus());
		} catch (MessageException e) {
			fail("unexpected");
			e.printStackTrace();
		} catch (IOException e) {
			fail("unexpected");
			e.printStackTrace();
		} catch (RecipientTypeException e) {
			e.printStackTrace();
			fail("unexpected");
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testGetMessageReportIsSuccessful(){
	    stubFor(get(urlEqualTo(StringUtils.GET_MESSAGE_REPORT+"/"+key+"/"+secret+"/ho0is4xn65f2g9256ss"))
	            .willReturn(aResponse()
	                .withStatus(200)));
		Response report = textMessagingService.getMessageReport("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe", "ho0is4xn65f2g9256ss");
		assertEquals(200,report.getStatus());
	}
	
	@Test
	public void testGetBalanceIsSuccessful(){
	    stubFor(get(urlEqualTo(StringUtils.BALANCE_URI+"/"+key+"/"+secret))
	    		.willReturn(okJson("{ \"balance\": \"100\" }")));
		Response balance = textMessagingService.getBalance("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe");
		assertEquals("{ \"balance\": \"100\" }",balance.readEntity(String.class));
	}
	
	@Test
	public void testGetCreditIsSuccessful(){
	    stubFor(get(urlEqualTo(StringUtils.CREDIT_URI+"/"+key+"/"+secret))
	    		.willReturn(okJson("{ \"credit\": \"100\" }")));
		Response credit = textMessagingService.getCredit("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe");
		assertEquals("{ \"credit\": \"100\" }",credit.readEntity(String.class));
	}
	
	@Test
	public void testCreateGroupWithNamedContactsIsSuccessful(){
		String randomGroupName = UUID.randomUUID().toString();
		String body = "{\"key\":\"gg0hs68y\",\"secret\":\"e5087bac141897ab37af89bae8d5dc1447ab4efe\","
				+ "\"groupName\":\""+randomGroupName+"\","
				+ "\"contacts\":[{\"number\":\"447789128763\",\"name\":\"James Renato\"},{\"number\":\"447955683364\",\"name\":\"Jasper Rhodes\"}]}";
		
		stubFor(post(urlEqualTo(StringUtils.GROUPS_URI))
				.withHeader("Accept", equalTo("application/json"))
	    		.withRequestBody(containing(body))
	    		.willReturn(created()));
		
		try{
			ArrayList<Contact> contactList = new ArrayList<Contact>();
			contactList.add(new NamedContact("James Renato", "447789128763"));
			ContactGroup g = textMessagingService.createContactGroupInstance("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe", randomGroupName);
			g.addContactList(contactList);
			g.addContact(new NamedContact("Jasper Rhodes", "447955683364"));
			Response r = textMessagingService.addNewGroup(g);
			assertEquals(201,r.getStatus());
		} catch (IOException e) {
			e.printStackTrace();
		} catch(ContactException e){
			e.printStackTrace();
			fail("Unexpected");
		}
	}
	
	
	@Test
	public void testCreateGroupWithNamedContactsAndUnnamedContactsIsSuccessful(){
		String randomGroupName = UUID.randomUUID().toString();
		String body = "{\"key\":\"gg0hs68y\",\"secret\":\"e5087bac141897ab37af89bae8d5dc1447ab4efe\","
				+ "\"groupName\":\""+randomGroupName+"\","
				+ "\"contacts\":[{\"number\":\"447789128763\",\"name\":\"James Renato\"},"
				+ "{\"number\":\"447900385585\"},{\"number\":\"447955683364\",\"name\":\"Jasper Rhodes\"}]}";
		
		stubFor(post(urlEqualTo(StringUtils.GROUPS_URI))
				.withHeader("Accept", equalTo("application/json"))
	    		.withRequestBody(containing(body))
	    		.willReturn(created()));
		try{
			ArrayList<Contact> contactList = new ArrayList<Contact>();
			contactList.add(new NamedContact("James Renato", "447789128763"));
			contactList.add(new Contact("447900385585"));
			ContactGroup g = textMessagingService.createContactGroupInstance("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe", randomGroupName);
			g.addContactList(contactList);
			g.addContact(new NamedContact("Jasper Rhodes", "447955683364"));
			Response r = textMessagingService.addNewGroup(g);
			assertEquals(201,r.getStatus());
		} catch (IOException e) {
			e.printStackTrace();
		} catch(ContactException e){
			e.printStackTrace();
			fail("Unexpected");
		}
	}
	
	
	@Test
	public void testCreateGroupContactsIsSuccessful(){
		String randomGroupName = UUID.randomUUID().toString();
		String body = "{\"key\":\"gg0hs68y\",\"secret\":\"e5087bac141897ab37af89bae8d5dc1447ab4efe\",\"groupName\":\""+randomGroupName+"\",\"contacts\":[{\"number\":\"447789128763\"}]}";
		stubFor(post(urlEqualTo(StringUtils.GROUPS_URI))
				.withHeader("Accept", equalTo("application/json"))
	    		.withRequestBody(containing(body))
	    		.willReturn(created()));
		try{
			ArrayList<Contact> contactList = new ArrayList<Contact>();
			contactList.add(new Contact("447789128763"));
			ContactGroup g = textMessagingService.createContactGroupInstance("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe", randomGroupName);
			g.addContactList(contactList);
			Response r = textMessagingService.addNewGroup(g);
			assertEquals(201,r.getStatus());
		} catch (IOException e) {
			e.printStackTrace();
		} catch(ContactException e){
			e.printStackTrace();
			fail("Unexpected");
		}
	}
	
	@Test
	public void testAddContactToGroupIsSuccessful(){
		String randomGroupName = UUID.randomUUID().toString();
		String body = "{\"key\":\"gg0hs68y\",\"secret\":\"e5087bac141897ab37af89bae8d5dc1447ab4efe\",\"groupName\":\""+randomGroupName+"\",\"contacts\":[{\"number\":\"447789128763\"}]}";
		stubFor(post(urlEqualTo(StringUtils.CONTACTS_URI))
				.withHeader("Accept", equalTo("application/json"))
	    		.withRequestBody(containing(body))
	    		.willReturn(created()));
		try{
			ArrayList<Contact> contactList = new ArrayList<Contact>();
			contactList.add(new Contact("447789128763"));
			ContactGroup g = textMessagingService.createContactGroupInstance("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe", randomGroupName);
			g.addContactList(contactList);
			assertEquals(201, textMessagingService.addContactsToGroup(g).getStatus());
		} catch (IOException e) {
			e.printStackTrace();
		} catch(ContactException e){
			e.printStackTrace();
			fail("Unexpected");
		}		
	}
	
	@Test
	public void testDeleteGroupIsSuccessful(){
	    stubFor(delete(urlEqualTo(StringUtils.GROUPS_URI+"/"+key+"/"+secret+"/283d81d0-93d5-4f9f-9277-ea40dfb18878"))
	    		.willReturn(ok()));
		Response deleted = textMessagingService.deleteGroup("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe","283d81d0-93d5-4f9f-9277-ea40dfb18878");
		assertEquals(200, deleted.getStatus());
	}
	
	@Test
	public void testDeleteGroupContactIsSuccessful(){
	    stubFor(delete(urlEqualTo(StringUtils.CONTACTS_URI+"/"+key+"/"+secret+"/18fe09f2-2746-40f1-9ee6-a34e8c8697b1"+"/447789128763"))
	    		.willReturn(ok()));
		Response deleted = textMessagingService.deleteContactFromGroup("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe","18fe09f2-2746-40f1-9ee6-a34e8c8697b1","447789128763");
		assertEquals(200,deleted.getStatus());
	}
	
	@Test
	public void testGetAllGroupsIsSuccessful(){
	    stubFor(get(urlEqualTo(StringUtils.GET_ALL_GROUPS_URI+"/"+key+"/"+secret))
	    		.willReturn(ok()));
		Response allGroups;
		allGroups = textMessagingService.getAllGroups("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe");
		assertEquals(200,allGroups.getStatus());
	}
	
	@Test
	public void testGetAllGroupContactsIsSuccessful(){
	    stubFor(get(urlEqualTo(StringUtils.GET_ALL_GROUP_CONTACTS_URI+"/"+key+"/"+secret+"/JandD"))
	    		.willReturn(okJson("{}")));
		Response allContacts;
		allContacts = textMessagingService.getAllGroupContacts("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe", "JandD");
		assertFalse(allContacts.readEntity(String.class).isEmpty());
	}
	
	@Test
	public void testGetDeliveryStatus(){
	    stubFor(get(urlEqualTo(StringUtils.GET_MESSAGE_DELIVERY_STATUS+"/"+key+"/"+secret+"/fqbiyrmy0ws2pl4s"))
	    		.willReturn(okJson("{}")));
		Response deliveryStatusResponse;
		deliveryStatusResponse = textMessagingService.getMessageDeliveryStatus("gg0hs68y", "e5087bac141897ab37af89bae8d5dc1447ab4efe", "fqbiyrmy0ws2pl4s");
		assertEquals(200,deliveryStatusResponse.getStatus());
	}
}
