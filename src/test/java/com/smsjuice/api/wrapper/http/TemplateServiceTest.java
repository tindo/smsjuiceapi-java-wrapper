package com.smsjuice.api.wrapper.http;

import java.io.IOException;
import java.util.UUID;

import javax.ws.rs.core.Response;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smsjuice.api.wrapper.TextMessagingService;
import com.smsjuice.api.wrapper.TextMessagingServiceFactory;
import com.smsjuice.api.wrapper.domain.model.MessageTemplate;
import com.smsjuice.api.wrapper.domain.model.StringUtils;
import com.smsjuice.api.wrapper.exception.TemplateException;

public class TemplateServiceTest extends AbstractServiceTest{
	
	protected static final ObjectMapper MAPPER = new ObjectMapper();

	private TextMessagingService textMessagingService = TextMessagingServiceFactory.getTestInstance();
	
	@Before
	public void setup(){
		
	}
	
	
	@Test
	public void testAddGetTemplateReturnsOK() throws TemplateException, IOException{
	    stubFor(post(StringUtils.TEMPLATES_URI)
	    		.withHeader("Accept", equalTo("application/json"))
	            .willReturn(aResponse()
	                .withStatus(201)));
		String uuid = UUID.randomUUID().toString();
		MessageTemplate m = new MessageTemplate(key, secret, "Hello, I am a template " + uuid);

		Response result = textMessagingService.addNewTemplate(m);
		assertEquals(Response.Status.CREATED.getStatusCode(), result.getStatus());
	}
	
	@Test
	public void testAddExistingTemplateReturnsCOnflict() throws TemplateException, IOException{
	    stubFor(post(StringUtils.TEMPLATES_URI)
	    		.withHeader("Accept", equalTo("application/json"))
	            .willReturn(aResponse()
	                .withStatus(409)));
		MessageTemplate m = new MessageTemplate(key, secret, "Hello, I am a template ");

		Response result = textMessagingService.addNewTemplate(m);
		Response repeatResult = textMessagingService.addNewTemplate(m);
		assertEquals(Response.Status.CONFLICT.getStatusCode(), repeatResult.getStatus());
	}
	
	
	@Test
	public void testAddTemplateWithEmptyMessageReturnsException(){
		try{
		MessageTemplate m = new MessageTemplate(key, secret, "");
		fail("Exception must be thrown");
		}catch(Exception e){
			//pass
		}
	}

	
	@Test
	public void testAddTemplateWithNullMessageReturnsException(){
		try{
		MessageTemplate m = new MessageTemplate(key, secret, null);
		fail("Exception must be thrown");
		}catch(Exception e){
			//pass
		}
	}
}
