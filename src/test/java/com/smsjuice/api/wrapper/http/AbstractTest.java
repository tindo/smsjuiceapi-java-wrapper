package com.smsjuice.api.wrapper.http;

import org.junit.Rule;

import com.github.tomakehurst.wiremock.junit.WireMockRule;

public abstract class AbstractTest {

	@Rule
	public WireMockRule wireMockRule = new WireMockRule(8021);
}
