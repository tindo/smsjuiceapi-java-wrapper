package com.smsjuice.api.wrapper.http;

import org.junit.Rule;

import com.github.tomakehurst.wiremock.junit.WireMockRule;

public abstract class AbstractServiceTest {
 
	
	protected String key = "gg0hs68y";
	protected String secret =  "e5087bac141897ab37af89bae8d5dc1447ab4efe";
	
	@Rule
	public WireMockRule wireMockRule = new WireMockRule(8021);

}
