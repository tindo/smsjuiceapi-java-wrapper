package com.smsjuice.api.wrapper.http;

import java.io.IOException;
import java.util.UUID;

import javax.ws.rs.core.Response;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.patch;
import static com.github.tomakehurst.wiremock.client.WireMock.delete;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smsjuice.api.wrapper.TextMessagingService;
import com.smsjuice.api.wrapper.TextMessagingServiceFactory;
import com.smsjuice.api.wrapper.domain.model.MessageTemplate;
import com.smsjuice.api.wrapper.domain.model.RecipientLimitRule;
import com.smsjuice.api.wrapper.domain.model.StringUtils;
import com.smsjuice.api.wrapper.exception.LimitRuleException;
import com.smsjuice.api.wrapper.exception.TemplateException;

public class RateLimitServiceTest extends AbstractServiceTest{


	private TextMessagingService textMessagingService = TextMessagingServiceFactory.getTestInstance();
	
	@Test
	public void testPostNewRuleReturnsCREATED() throws LimitRuleException, IOException{
	    stubFor(post(StringUtils.RECIPIENT_RATE_LIMITS_URI)
	    		.withHeader("Accept", equalTo("application/json"))
	            .willReturn(aResponse()
	                .withStatus(201)));
		RecipientLimitRule r = new RecipientLimitRule(key, secret, 1, 1);
		Response response = textMessagingService.addRecipientRateLimitRule(r);
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
	}
	
	
	@Test
	public void testDeleteRuleReturnsOK(){
	    stubFor(delete(urlEqualTo(StringUtils.REMOVE_RECIPIENT_RATE_LIMITS_URI+"/"+key+"/"+secret))
	            .willReturn(aResponse()
	                .withStatus(200)));
		Response r = textMessagingService.deleteRecipientRateLimitRule(key, secret);
		assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
	}
	
	@Test
	public void testCreateNewRuleWithZeroValuesThrowsException(){
	    stubFor(get(urlEqualTo(StringUtils.RECIPIENT_RATE_LIMITS_URI+"/"+key+"/"+secret))
	            .withHeader("Accept", equalTo("application/json"))
	            .willReturn(aResponse()
	                .withStatus(400)));
		try {
			RecipientLimitRule r = new RecipientLimitRule(key, secret, 0, 1);
			fail("must throw exception because of zero-value argument");
		} catch (LimitRuleException e) {
			//expected
		}
	}
	
	
}
