package com.smsjuice.api.wrapper;

/**
 * @author Wilson Mazaiwana - SMSJuice Limited
 */

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smsjuice.api.wrapper.domain.model.ContactGroup;
import com.smsjuice.api.wrapper.domain.model.Message;
import com.smsjuice.api.wrapper.domain.model.MessageTemplate;
import com.smsjuice.api.wrapper.domain.model.RecipientLimitRule;
import com.smsjuice.api.wrapper.domain.model.RecipientType;
import com.smsjuice.api.wrapper.domain.model.StringUtils;
import com.smsjuice.api.wrapper.exception.ContactException;
import com.smsjuice.api.wrapper.exception.LimitRuleException;
import com.smsjuice.api.wrapper.exception.MessageException;
import com.smsjuice.api.wrapper.exception.RecipientTypeException;
import com.smsjuice.api.wrapper.http.HTTPRequest;

public class TextMessagingService {
	
	private String url = "https://api.smsjuice.com";
	protected  final ObjectMapper mapper = new ObjectMapper();
	
	
	public TextMessagingService() {
	}
	
	public TextMessagingService(String url) {
		this.url = url;
	}
	
	
	/**
	 * Creates an instance of a message object which stores all aspects of the message (from, to-list, receiptwanted, message-payload,, encoding, sending schedule date, expiry date of the message, udhi )
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @return an instance of Message
	 * @throws MessageException
	 * @throws RecipientTypeException
	 * @deprecated 
	 */
	@Deprecated
	public  Message createMessageInstance(String key, String secret,String recipientType) throws MessageException, RecipientTypeException{
		return new Message(key, secret, recipientType);
	}
	
	
	/**
	 * Creates an instance of a message object which stores all aspects of the message (from, to-list, receiptwanted, encoding, sending schedule date, expiry date of the message, udhi )
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @param recipientType Type of recipients to which the message will be sent. Groups or numbers
	 * @param templateId the template ID of the message (stored on your SMSJuice account) to be sent as the message content to the recipients
	 * @return an instance of Message
	 * @throws MessageException
	 * @throws RecipientTypeException 
	 */
	public  Message createMessageInstance(String key, String secret,
			RecipientType recipientType, int templateId) throws MessageException {
		return new Message(key, secret, recipientType, templateId);
	}
	

	/**
	 * Creates an instance of a message object which stores all aspects of the message (from, to-list, receiptwanted, message-content, encoding, sending schedule date, expiry date of the message, udhi )
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @param recipientType Type of recipients to which the message will be sent. Groups or numbers
	 * @param message The message content to be sent
	 * @return an instance of Message
	 * @throws MessageException
	 * @throws RecipientTypeException 
	 */
	public  Message createMessageInstance(String key, String secret,
			RecipientType recipientType, String message) throws MessageException {
		return new Message(key, secret, recipientType, message);
	}
	
	
	/**
	 * Creates an instance of a group in which you can add contacts
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @param groupName The name that will be given to the group
	 * @return an instance of the ContactGroup
	 * @throws ContactException
	 */
	public  ContactGroup createContactGroupInstance(String key, String secret, String groupName) throws ContactException{
		return new ContactGroup(key, secret, groupName);
	}
	
	
	/**
	 * Sends a message to the server API to be sent to all the recipients listed in the message object
	 * @param message Message object to send
	 * @return A Response object with JSON string response from the server with the result
	 * @throws MessageException
	 * @throws IOException
	 */
	public  Response sendMessage(Message message)throws IOException{
		String messageJSON = mapper.writeValueAsString(message);
		return HTTPRequest.post("/send",messageJSON, url);
	}
	
	/**
	 * Returns the message status of a message sent.
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @param messageId ID of the message that you want to query to see its delivery status or report.
	 * @return A Response object with JSON string with the report on the status of the message
	 * @throws IOException
	 */
	public  Response getMessageReport(String key, String secret, String messageId) {
		Map<String, String> pathArgs = new LinkedHashMap<>();
		pathArgs.put("key", key);
		pathArgs.put(StringUtils.SECRET_KEYWORD, secret);
		pathArgs.put("messageId", messageId);
		return HTTPRequest.get(StringUtils.GET_MESSAGE_REPORT, pathArgs, url);
	}
	
	/**
	 * Gets the balance on your account
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @return A Response object with JSON string response from the server with the result
	 * @throws IOException
	 */
	public  Response getBalance(String key, String secret) {
		Map<String, String> pathArgs = new LinkedHashMap<>();
		pathArgs.put("key", key);
		pathArgs.put(StringUtils.SECRET_KEYWORD, secret);
		return HTTPRequest.get(StringUtils.BALANCE_URI, pathArgs, url);
	}
	
	/**
	 * Gets the credits left on your account
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @return A Response object with JSON string response from the server with the result
	 * @throws IOException
	 */
	public  Response getCredit(String key, String secret) {
		Map<String, String> pathArgs = new LinkedHashMap<>();
		pathArgs.put("key", key);
		pathArgs.put(StringUtils.SECRET_KEYWORD, secret);
		return HTTPRequest.get(StringUtils.CREDIT_URI, pathArgs, url);
	}

	/**
	 * Adds a new contact group onto your account as one would add from the SMSJuice web account. It can be empty
	 * @param group the ContactGroup to add to your account
	 * @return A Response object with JSON string indicating the success status
	 * @throws ContactException
	 * @throws IOException
	 */
	public  Response addNewGroup(ContactGroup group) throws IOException{
		String groupJSON = mapper.writeValueAsString(group);
		return HTTPRequest.post(StringUtils.GROUPS_URI,groupJSON, url);
	}

	/**
	 * Add new contacts to a group of contacts. In a way it's editing a group.
	 * @param group groups to add contacts to
	 * @return A Response object with JSON string indicating the success status 
	 * @throws ContactException
	 * @throws IOException
	 */
	public  Response addContactsToGroup(ContactGroup group) throws IOException{
		String groupJSON = mapper.writeValueAsString(group);
		return HTTPRequest.post(StringUtils.CONTACTS_URI,groupJSON, url);
	}
	
	/**
	 * Get a list of all groups from your SMSJuice account 
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @return A Response object with JSON string with a list of ALL groups on your account
	 * @throws IOException
	 */
	public  Response getAllGroups(String key, String secret) {
		Map<String, String> pathArgs = new LinkedHashMap<>();
		pathArgs.put("key", key);
		pathArgs.put(StringUtils.SECRET_KEYWORD, secret);
		return HTTPRequest.get(StringUtils.GET_ALL_GROUPS_URI, pathArgs, url);
	}
	
	/**
	 * Gets all contacts in a group
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @param groupName the group name whose contacts you want to get
	 * @return A Response object with JSON string of the contacts
	 * @throws IOException
	 */
	public  Response getAllGroupContacts(String key, String secret, String groupName) {
		Map<String, String> pathArgs = new LinkedHashMap<>();
		pathArgs.put("key", key);
		pathArgs.put(StringUtils.SECRET_KEYWORD, secret);
		pathArgs.put(StringUtils.GROUPNAME_KEYWORD, groupName);
		return HTTPRequest.get(StringUtils.GET_ALL_GROUP_CONTACTS_URI, pathArgs, url);
	}

	/**
	 * Deletes a group from your SMSJuice account
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @param groupName the group name you wish to delete
	 * @return A Response object with JSON string with confirmation of deletion
	 */
	public  Response deleteGroup(String key, String secret,
			String groupName) {
		Map<String, String> pathArgs = new LinkedHashMap<>();
		pathArgs.put("key", key);
		pathArgs.put(StringUtils.SECRET_KEYWORD, secret);
		pathArgs.put(StringUtils.GROUPNAME_KEYWORD, groupName);
		return HTTPRequest.delete(StringUtils.GROUPS_URI, pathArgs, url);
	}
	
	
	/**
	 * Deletes a contact from a group. Note that at this point you can only delete by numbers, not by names
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @param groupName the group name you wish to delete the contact from
	 * @param contactNumber the number you wish to remove from the group
	 * @return A Response object with JSON string with confirmation of deletion
	 */
	public  Response deleteContactFromGroup(String key, String secret,
			String groupName, String contactNumber) {
		Map<String, String> pathArgs = new LinkedHashMap<>();
		pathArgs.put("key", key);
		pathArgs.put(StringUtils.SECRET_KEYWORD, secret);
		pathArgs.put(StringUtils.GROUPNAME_KEYWORD, groupName);
		pathArgs.put("contactNumber", contactNumber);
		return HTTPRequest.delete(StringUtils.CONTACTS_URI, pathArgs, url);
	}
	
	
	/**
	 * Adds a new canned-message/template for sending to the recipient.
	 * @param template the template with the message to add
	 * @return A Response object with JSON string with confirmation of creation
	 * @throws ContactException
	 * @throws IOException
	 */
	public  Response addNewTemplate(MessageTemplate template) throws IOException{
		String templateJSON = mapper.writeValueAsString(template);
		return HTTPRequest.post(StringUtils.TEMPLATES_URI,templateJSON, url);
	}
	
	
	/**
	 * Get a list of all templates stored for your account
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @return A Response object with JSON string with all the templates stored on your account
	 */
	public  Response getTemplatesList(String key, String secret){
		Map<String, String> pathArgs = new LinkedHashMap<>();
		pathArgs.put("key", key);
		pathArgs.put(StringUtils.SECRET_KEYWORD, secret);
		return HTTPRequest.get(StringUtils.GET_ALL_TEMPLATES_URI, pathArgs, url);
	}
	
	
	
	/**
	 * Get one  template stored on your account by its Id
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @param templateId ID of the remplate you want to get
	 * @return A Response object with JSON string with the queried template
	 */
	public  Response getTemplate(String key, String secret, String templateId){
		Map<String, String> pathArgs = new LinkedHashMap<>();
		pathArgs.put("key", key);
		pathArgs.put(StringUtils.SECRET_KEYWORD, secret);
		pathArgs.put("templateId", templateId);
		return HTTPRequest.get(StringUtils.GET_SINGLE_TEMPLATE_URI, pathArgs, url);
	}
	
	
	/**
	 * Sets a rate limit rule for recipients. There's only ever one maximum rule for this (or zero if not set), so putting a new rule replaces the old one.
	 * @param rule Rule object with the rate limit rules
	 * @return A Response object with JSON string with the confirmation of the operation
	 * @throws LimitRuleException Thrown when either of the set rules is less than the numeric value of 1
	 * @throws IOException
	 */
	public  Response addRecipientRateLimitRule(RecipientLimitRule rule) throws LimitRuleException, IOException{
		String ruleJSON = mapper.writeValueAsString(rule);
		return HTTPRequest.post(StringUtils.RECIPIENT_RATE_LIMITS_URI,ruleJSON, url);
	}
	
	
	
	
	/**
	 * Delete/Reomove recipient rate limit rules
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @return A Response object with JSON string with the confirmation of the operation
	 */
	public  Response deleteRecipientRateLimitRule(String key, String secret) {
		Map<String, String> pathArgs = new LinkedHashMap<>();
		pathArgs.put("key", key);
		pathArgs.put(StringUtils.SECRET_KEYWORD, secret);
		return HTTPRequest.delete(StringUtils.REMOVE_RECIPIENT_RATE_LIMITS_URI, pathArgs, url);
	}
	
	/**
	 * Get the delivery status of a message
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @param messageId - ID of the message you are querying.
	 * @return A response with JSON string with the delivery status of the message queried
	 */
	public  Response getMessageDeliveryStatus(String key, String secret, String messageId){
		Map<String, String> pathArgs = new LinkedHashMap<>();
		pathArgs.put("key", key);
		pathArgs.put(StringUtils.SECRET_KEYWORD, secret);
		pathArgs.put("messageId", messageId);
		return HTTPRequest.get(StringUtils.GET_MESSAGE_DELIVERY_STATUS, pathArgs, url);
	}
}
