package com.smsjuice.api.wrapper.http;

import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HTTPRequest {

	private static Logger logger = LoggerFactory.getLogger(HTTPRequest.class);
	
	private HTTPRequest() {}

    private static WebTarget globalTarget;
        
    public static Response post(String endpoint, String json, String url){
    	Client client = ClientBuilder.newClient();
    	WebTarget target = client.target(url + endpoint);
    	Invocation.Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);
    	return invocationBuilder.post(Entity.entity(json, MediaType.APPLICATION_JSON));
    }
    
    public static Response get(String endpoint, Map<String, String> pathArgs, String url){
    	//reset globalTarget
    	globalTarget = null;
    	Client client = ClientBuilder.newClient();
    	globalTarget = client.target(url + endpoint);
    	
    	pathArgs.forEach((k,v)->globalTarget = globalTarget.path(v));
    	logger.debug(globalTarget.getUri().getPath());
    	Invocation.Builder invocationBuilder = globalTarget.request(MediaType.APPLICATION_JSON);
    	return invocationBuilder.get();
    	
    }
   
    public static Response delete(String endpoint, Map<String, String> pathArgs, String url){
    	//reset globalTarget
    	globalTarget = null;
    	Client client = ClientBuilder.newClient();
    	globalTarget = client.target(url + endpoint);
    	
    	pathArgs.forEach((k,v)->globalTarget = globalTarget.path(v));
    	logger.debug(globalTarget.getUri().getPath());
    	Invocation.Builder invocationBuilder = globalTarget.request();
    	return invocationBuilder.delete();
    }

}
