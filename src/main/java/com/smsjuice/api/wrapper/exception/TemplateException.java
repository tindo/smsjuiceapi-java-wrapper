package com.smsjuice.api.wrapper.exception;

public class TemplateException extends Exception {
	public TemplateException(String message) {
		super(message);
	}
}
