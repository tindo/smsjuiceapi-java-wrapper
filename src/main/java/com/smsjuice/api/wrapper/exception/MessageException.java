package com.smsjuice.api.wrapper.exception;

public class MessageException extends Exception{

	public MessageException(String message){
		super(message);
	}
}
