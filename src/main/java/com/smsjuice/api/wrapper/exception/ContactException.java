package com.smsjuice.api.wrapper.exception;

public class ContactException extends Exception{
	
	public ContactException(String message) {
		super(message);
	}
}
