package com.smsjuice.api.wrapper.exception;

public class RecipientTypeException extends Exception {
	public RecipientTypeException(String message) {
		super(message);
	}
}
