package com.smsjuice.api.wrapper.exception;

public class LimitRuleException extends Exception {

	public LimitRuleException(String message) {
		super(message);
	}
}
