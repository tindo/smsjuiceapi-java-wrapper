package com.smsjuice.api.wrapper.domain.model;

import com.smsjuice.api.wrapper.exception.ContactException;

public class NamedContact extends Contact {

	private String name;
	
	public NamedContact(String name, String number) throws ContactException {
		super(number);
		this.number = number;
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
}
