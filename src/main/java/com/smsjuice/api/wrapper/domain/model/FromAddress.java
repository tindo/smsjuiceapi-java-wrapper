package com.smsjuice.api.wrapper.domain.model;

import com.smsjuice.api.wrapper.exception.MessageException;
/**
 * Source Address is a bit more complicated than the destination address, since one can put in both numeric values or alphanumeric values
 * @author Wilson Mazaiwana
 *
 */
public class FromAddress {
	
	private String address;

	public FromAddress(String address) throws MessageException{
		if (null == address){
			throw new MessageException("Exeption: Null 'Fo Address' submitted.");
		}
		
		address = cleanAddress(address);
		
        if(address.matches(StringUtils.NUMERIC_STRING)){
            if(address.length()>15){
            	throw new MessageException("Exeption: Invalid 'From Address'.");
            }
        } else if (address.matches(StringUtils.ALPHA_NUMERIC_STRING)){
            if(address.length()>11){
            	throw new MessageException("Exeption: Address too long to be alphanumeric. Maximum 11 alphanumeric (A-Za-z0-9) characters requied.");
            }
        } else {
        	throw new MessageException("Exeption: Invalid 'From Address' submitted.");
        }
        
        this.address = address;
        
	}

	private String cleanAddress(String address) {
		address = address.trim();
		if(address.startsWith("+")){
			address = address.replaceFirst("\\+", "");
		}
		
		if(address.startsWith("00") && address.matches(StringUtils.NUMERIC_STRING) && address.length()>11){
			address = address.replaceFirst("00", "");
		}
		return address;
	}
	
	public String getAddress(){
		return this.address;
	}
}
