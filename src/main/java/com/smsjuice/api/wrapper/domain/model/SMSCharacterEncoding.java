package com.smsjuice.api.wrapper.domain.model;

import com.smsjuice.api.wrapper.exception.MessageException;

/**
 * 
 * @author Wilson Mazaiwana
 *
 */
public enum SMSCharacterEncoding {
	SMSC_DEFAULT(0),ASCII(1),UNSPEC1(2),LATIN(3),UNSPEC2(4),JIS(5),CYRLLIC(6),LATINHEBREW(7),UCS2(8);
	private final int encoding;
	
	/**
	 * Returns the character encoding to send with the message. 
	 * @param encoding The encoding caluse to use. Recommendation is to use 0, 3 or 8 as values as they are
	 *  the most commonly used. The default is 0 if one doesn't select which one they want to use, and it usually suffices.
	 * @return The character encoding to send with the message.
	 * @throws MessageException triggered when one tries to instansiate the object with a value less than 0 or grater than 8 .
	 */
	public static SMSCharacterEncoding instanceFromValue(int encoding) throws MessageException{
		
		switch(encoding){
		case 0:
			return SMSC_DEFAULT;
		case 1:
			return ASCII;
		case 2:
			return UNSPEC1;
		case 3:
			return LATIN;
		case 4:
			return UNSPEC2;
		case 5:
			return JIS;
		case 6:
			return CYRLLIC;
		case 7:
			return LATINHEBREW;
		case 8:
			return UCS2;			
		default:
			throw new MessageException("Encoding can only be an integer between 0 and 8");
		}
	}
	
	private SMSCharacterEncoding(int encoding){
		this.encoding = encoding;
	}

	
	public int getEncoding(){
		return this.encoding;
	}
}
