package com.smsjuice.api.wrapper.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.smsjuice.api.wrapper.exception.RecipientTypeException;

public enum RecipientType {
	
	/**
	 * Indicator for sending messages to groups on your SMSJuice account. This means the message will have groups to send to instead of numbers in the "to" field/list
	 */
	GROUPS("groups"),
	
	/**
	 * For sending messages to mobile phone numbers. This means the message object will have a list of numbers to send to.
	 */
	NUMBERS("numbers");
	
	@JsonProperty
	private final String reciptientTypeValue;
	
	/**
	 * Returns the type of recipient to send the message to.
	 *  When one selects 'groups' they would put a list of existing groups on their SMSJuice account to send the message to.
	 *  When one select 'numbers' they would put a list of mobile phone numbers to send the message to.
	 * @param reciptientTypeValue  It has to be either 'groups' OR 'numbers'.
	 * @return Type of recipient to send the message to.
	 * @throws RecipientTypeException when anything other than 'groups' OR 'numbers' has been used to try to create the ENUM object
	 */
	public static RecipientType instanceFromValue(String recipientTypeValue) throws RecipientTypeException{
		if(recipientTypeValue==null || recipientTypeValue.trim().isEmpty()){
			throw new RecipientTypeException("Invalid recipient type. It has to be either 'groups' OR 'numbers'.");
		}
		
		switch(recipientTypeValue.trim().toLowerCase()){
			case "groups":
				return GROUPS;
			case "numbers":
				return NUMBERS;
			default:
				throw new RecipientTypeException("Invalid recipient type. It has to be either 'groups' OR 'numbers'.");
		}
	}
	
	private RecipientType(String reciptientTypeValue){
		this.reciptientTypeValue = reciptientTypeValue;
	}
	
	@JsonValue
	@JsonProperty
	public String getRecipientTypeValue(){
		return this.reciptientTypeValue;
	}
		

}
