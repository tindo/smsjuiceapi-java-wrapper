package com.smsjuice.api.wrapper.domain.model;

/**
 * @author Wilson Mazaiwana
 */

import java.util.ArrayList;
import java.util.List;

import com.smsjuice.api.wrapper.exception.ContactException;

public class ContactGroup {

	private String key;
	private String secret;
	private String groupName;
	private List<Contact> contacts = new ArrayList<>();
	
	/**
	 * 
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @param groupName name of the group that ContactGroup represents
	 * @throws ContactException
	 */
	public ContactGroup(String key, String secret, String groupName) throws ContactException {
		if(null == key || key.isEmpty() || null == secret || secret.isEmpty() || null == groupName || groupName.isEmpty()){ //maybe reflection is better to use here?
			throw new ContactException("Null/Empty value in key or secret");
		}
		this.key = key;
		this.secret = secret;
		this.groupName = groupName;
	}
	
	/**
	 * Add a list of contacts (with or without names) to the ContactGroup
	 * @param contacts numbers (or names & numbers) to be added
	 * @return ContactGroup object
	 */
	public ContactGroup addContactList(List<Contact> contacts){
		this.contacts = contacts;
		return this;
	}
	
	/**
	 * Add single contact to the COntactGroup object
	 * @param contact contact to be added
	 * @return ContactGroup object
	 */
	public ContactGroup addContact(Contact contact){
		this.contacts.add(contact);
		return this;
	}

	public String getKey() {
		return key;
	}

	public String getSecret() {
		return secret;
	}

	/**
	 * Returns the name of the group represented by this ContactGroup object
	 * @return Name of group returned
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * Returns the list of contacts contained within the ContactGroup object.
	 * @return List which may contain Strings of numbers for the contacts, or Contact objects containing the name and number of the contacts
	 */
	public List<Contact> getContacts() {
		return contacts;
	}
	
}
