package com.smsjuice.api.wrapper.domain.model;

import com.smsjuice.api.wrapper.exception.ContactException;

/**
 * To Address. Address will only accept international numbers and normal looking phone numbers. No shortcodes allowed at this time as the system does not send messages to shortcodes
 * 
 * @author Wilson Mazaiwana
 *
 */
public class Contact {

	private static final String TO_ADDRESS_MATCHER = "\\d+"; 
	
	
	protected String number;
	
	public Contact(String uncheckedAddress) throws ContactException{
		if (null == uncheckedAddress || uncheckedAddress.isEmpty()){
			throw new ContactException("Exeption: Null/Empty 'To address' submitted.");
		}
		
		uncheckedAddress = cleanAddress(uncheckedAddress);
		
		if(!uncheckedAddress.matches(TO_ADDRESS_MATCHER)  || uncheckedAddress.length()<6 || uncheckedAddress.length()>15){
			throw new ContactException("Exception: Invalid 'To address' submitted: "+uncheckedAddress);
		}
		
		this.number = uncheckedAddress;
	}
	

	public String getNumber(){
		return this.number;
	}
	
	/**
	 * Some may insist in leaving on trailing plus signs, so this will help any legitimate numbers with leading plus signs and double zeros to be formatted before seeing if they conform to the standard MSISDN format.
	 * @param address
	 * @return
	 */
	private String cleanAddress(String originalAddress) throws ContactException{
		String address = originalAddress.trim();
		if(address.startsWith("+")){
			address = address.replaceFirst("\\+", "");
		}
		if(address.startsWith("0")){
			if(address.startsWith("00")){
				address = address.replaceFirst("00", "");
			}else{
				throw new ContactException("Invalid contact number submitted: suspected missing international code." + originalAddress);
			}
		}
		
		//remove all spaces
		address = address.replaceAll(" ", "");
		
		return address;
	}
}
