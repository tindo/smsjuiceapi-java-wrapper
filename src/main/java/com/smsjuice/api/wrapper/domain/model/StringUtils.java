package com.smsjuice.api.wrapper.domain.model;

import java.time.format.DateTimeFormatter;

public class StringUtils {
	
	private StringUtils() {
	}

	public static final String NUMERIC_STRING = "\\d+";
	
	public static final String ALPHA_NUMERIC_STRING = "^[A-Za-z0-9]*$";
	
	public static final int UDH_OFF = 0;
	
	public static final int UDH_ON = 1;
	
	public static final int ENCODING_SMSC_DEFAULT = 0;
	
	public static final int ENCODING_ANSI = 1;
	
	public static final String EIGHTBIT_PREFIX = "\u0005\u0000\u0003";

	public static final String BALANCE_URI = "/balance";
	
	public static final String CREDIT_URI = "/credits";

	public static final String GROUPS_URI = "/groups";

	public static final String CONTACTS_URI = "/groups/contacts";

	public static final String GET_ALL_GROUPS_URI = "/groups/all";

	public static final String GET_ALL_GROUP_CONTACTS_URI = "/groups/contacts/all";
	
	public static final String GET_MESSAGE_REPORT = "/query";
	
	public static final String  TEMPLATES_URI = "/templates";
	
	public static final String  GET_ALL_TEMPLATES_URI = "/templates/all";
	
	public static final String  GET_SINGLE_TEMPLATE_URI = "/templates";
	
	public static final String  RECIPIENT_RATE_LIMITS_URI = "/limits/recipient";
	
	public static final String  REMOVE_RECIPIENT_RATE_LIMITS_URI = "/limits/recipient/remove";
	
	public static final String GET_MESSAGE_DELIVERY_STATUS = "/query";

	public static final String KEY_KEYWORD = "key";
	public static final String SECRET_KEYWORD = "secret";
	public static final String GROUPNAME_KEYWORD = "groupName";
	
	
	
	public static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm");
}
