package com.smsjuice.api.wrapper.domain.model;

import com.smsjuice.api.wrapper.exception.MessageException;


public enum ReceiptRequest{
	/**
	 * No Receipt needed
	 */
	NO_RECEIPT(0),
	/**
	 * Receipt Needed
	 */
	RECEIPT_WANTED(1);
	private final int receipt;
	
	public static ReceiptRequest instanceFromValue(int receipt) throws MessageException{
		switch(receipt){
		case 0:
			return NO_RECEIPT;
		case 1:
			return RECEIPT_WANTED;
		default:
			throw new MessageException("Receipt can only be 0 for No Receipt, or 1 for Receipt wanted.");
		}
	}
	
	private ReceiptRequest(int receipt){
		this.receipt = receipt;
	}
	
	public int getReceiptOption(){
		return this.receipt;
	}
	
	
}
