package com.smsjuice.api.wrapper.domain.model;

import org.apache.commons.lang3.Validate;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.smsjuice.api.wrapper.exception.LimitRuleException;

public class RecipientLimitRule {

	
	private String key;
	
	private String secret;
	
	@JsonProperty(value = "messagesLimit")
	private int messagesLimit;
	
	@JsonProperty(value = "timePeriodInMinutes")
	private int timePeriodInMinutes;
	
	

	/**
	 * Rule to set how many messages can be sent to a specific recipient within a specified time. There can only be one maximum rule per account. Setting a new one replaces the old one.
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @param messagesLimit Maximum number of messages to send within the specified time period. Must be greater than 0
	 * @param timePeriodInMinutes The period in which the messages limit applies. Must be greater than 0
	 * @throws LimitRuleException Thrown usually when one of the parameters is missing or when the primitive parameters (messagesLimit & timePeriodInMinutes) are less than 1.
	 */
	public RecipientLimitRule(String key, String secret, int messagesLimit, int timePeriodInMinutes) throws LimitRuleException{
		Validate.notEmpty(key);
		Validate.notEmpty(secret);
		if(messagesLimit < 1 || timePeriodInMinutes < 1){
			throw new LimitRuleException("'messagesLimit' and 'timePeriodInMinutes' MUST be greater tthan 0"); 
		}
		this.key = key;
		this.secret = secret;
		this.timePeriodInMinutes = timePeriodInMinutes;
		this.messagesLimit = messagesLimit;
	}

	public String getKey() {
		return key;
	}

	public String getSecret() {
		return secret;
	}

	public int getMessagesLimit() {
		return messagesLimit;
	}

	public int getTimePeriodInMinutes() {
		return timePeriodInMinutes;
	}
	
	
	
	
	
}
