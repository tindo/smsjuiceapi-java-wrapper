package com.smsjuice.api.wrapper.domain.model;

import org.apache.commons.lang3.Validate;

/**
 * 
 * @author Wilson Mazaiwana
 *
 */
public class MessageTemplate {

	private String key;

	private String secret;

	private String message;

	/**
	 * Message template to use in the future. Instead of typing a message, you can store this template and just use a message template ID in your messages
	 * @param key found from your account settings page
	 * @param secret found from your account settings page
	 * @param message
	 */
	public MessageTemplate(String key, String secret, String message) {
		Validate.notEmpty(key);
		Validate.notEmpty(secret);
		Validate.notEmpty(message);
		this.key = key;
		this.secret = secret;
		this.message = message;
	}

	public String getKey() {
		return key;
	}

	public String getSecret() {
		return secret;
	}

	public String getMessage() {
		return message;
	}
	
	
	
	
}
