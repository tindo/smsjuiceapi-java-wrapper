package com.smsjuice.api.wrapper.domain.model;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.smsjuice.api.wrapper.exception.ContactException;
import com.smsjuice.api.wrapper.exception.MessageException;
import com.smsjuice.api.wrapper.exception.RecipientTypeException;

/**
 * 
 * @author Wilson Mazaiwana
 *
 */
public class Message 
{
	private SMSCharacterEncoding encoding;
	
	private ReceiptRequest receipt;
	private String key;
	private String secret;
	private String from;
	private String message;
	private LocalDate schedule = null;
	private LocalDate expiry = null;
	
	private List<String> to = new ArrayList<>();

	private RecipientType recipientType;

	private int templateId;
	
	private static Logger logger = LoggerFactory.getLogger(Message.class);
	
	/**
	 * Constructor Takes key and secret which are provided in the settings page of your account
	 * @param key SMSJuice account key
	 * @param secret SMSJuice account secret
	 * @throws MessageException 
	 * @throws RecipientTypeException 
	 * @deprecated
	 */
	@Deprecated
	public Message(String key, String secret, String recipientType) throws MessageException, RecipientTypeException{
		if(null == key || key.isEmpty() || null == secret || secret.isEmpty()){
			throw new MessageException("Null/Empty value in key or secret");
		}
		this.key = key;
		this.secret = secret;
		this.recipientType = RecipientType.instanceFromValue(recipientType);
	}
	
	
	/**
	 * Constructor Takes key and secret which are provided in the settings page of your account
	 * @param key SMSJuice account key
	 * @param secret SMSJuice account secret
	 * @param recipientType Type of recipients to send to. On your SMSjuice account you have groups of numbers, and individual numbers. This distinguishes whether you are sending to groups or individual numbers
	 * @param messageContent The actual content of the message that will be read by the recipient(s) 
	 * @throws MessageException 
	 */
	public Message(String key, String secret, RecipientType recipientType, String messageContent) throws MessageException{
		Validate.notEmpty(key);
		Validate.notEmpty(secret);
		Validate.notEmpty(messageContent);
		Validate.notNull(recipientType);
		if(messageContent.length()>765){throw new MessageException("Exception: Message length can only be between 0 - 765 characters.");}
		this.key = key;
		this.secret = secret;
		this.message = messageContent;
		this.recipientType = recipientType;
	}
	
	
	
	/**
	 * Constructor Takes key and secret which are provided in the settings page of your account
	 * @param key SMSJuice account key
	 * @param secret SMSJuice account secret
	 * @param recipientType Type of recipients to send to. On your SMSjuice account you have groups of numbers, and individual numbers. This distinguishes whether you are sending to groups or individual numbers
	 * @param templateId the ID of the template stored on your SMSJuice account that you wish to use as message content 
	 */
	public Message(String key, String secret, RecipientType recipientType, int templateId) throws MessageException{
		Validate.notEmpty(key);
		Validate.notEmpty(secret);
		if(templateId<1){throw new MessageException("Template ID must be greater than 0");}
		Validate.notNull(recipientType);
		this.key = key;
		this.secret = secret;
		this.templateId = templateId;
		this.recipientType = recipientType;
	}
	
	
	
	/**
	 * Set the "from" address/number. This is what the person sees at the top of the message as the "from number". This can be up to 11 alpha-numeric characters OR up to 15 digits only. If it is an international number (MSISDN) the make sure it starts with the country code. No leading double zeros or leading plus. 
	 * @param from The "from" address/number
	 * @throws MessageException 
	 */
	public Message setFrom(String from) throws MessageException{
		this.from = new FromAddress(from).getAddress();
		return this;
	}
	
	/**
	 * For adding a new recipient to the list of recipients to receive messages.
	 * @param recipient mobile phone to receive the message
	 * @throws MessageException 
	 */
	public Message addRecipient(String recipient) throws ContactException{
		if(recipientType.equals(RecipientType.GROUPS)){
			to.add(recipient);
		}else{
			to.add(new Contact(recipient).getNumber());
		}
		return this;
	}
	
	/**
	 * For adding a list of recipients to receive the message to be sent
	 * @param to List of recipient phone numbers OR group names
	 * @return Message object
	 */
	public Message addRecipientList(List<String> to){
		//we would do an add all but we need to check the numbers being submitted
		if(recipientType.equals(RecipientType.GROUPS)){
			this.to.addAll(to);
		}else{
			for (String recip : to){
				try{
					this.to.add(new Contact(recip).getNumber());
				}catch(Exception e){
					logger.error(e.getMessage());
				}
			}
		}
		return this;
	}
	
	/**
	 * Sets the type of recipient, as user can send to a list of numbers, or a to list of groups on their account
	 * @param type Recipient type determining whether the recipient list is made of numbers or group names
	 * @return Message object
	 * @throws RecipientTypeException
	 */
	public Message setRecipientType(String type) throws RecipientTypeException{
		this.recipientType = RecipientType.instanceFromValue(type);
		return this;
	}

	
	
	/**
	 *  Sets the message content that is intended for the recipient to read. Content can be up to 765 characters (5 SMS)
	 * @param messageContent message content to send
	 * @return Message object
	 * @throws MessageException
	 * @deprecated
	 */
	@Deprecated
	public Message setMessage(String messageContent) throws MessageException{
		if( null==messageContent || messageContent.isEmpty() || messageContent.length()>765){
			throw new MessageException("Exception: Message must contain at least 1 not-null character, up to a maximum of 765 characters.");
		}
		if(this.templateId>0){
			throw new MessageException("Exception: the messageContent and the templateId cannot be both set. It has to be either the messageContent or the templateId");
		}
		
		this.message = messageContent;
		return this;
	}
	
	/**
	 * Optional - Sets whether a receipt is relayed to the provided callback url or not. 1 for yes , 0 for no. Default is 0
	 * @param receiptIndicator indicator for whether a receipt is needed or not
	 * @return Message object
	 * @throws MessageException 
	 */
	public Message setReceiptOption(int receiptIndicator) throws MessageException{
		this.receipt = ReceiptRequest.instanceFromValue(receiptIndicator);
		return this;
	}
	
	/**
	 * setSendDate - Optional - Sets date and time to send message. Time set to GMT+0/London time
	 * @param sendDate date and time to send the message - in 'yyyy-MM-dd hh:mm' format
	 * @throws MessageException
	 */
	public Message setSendDate(String sendDate) throws MessageException{
		if(null==sendDate){
			throw new MessageException("Invalid Send Date Format: Make sure it's in 'yyyy-MM-dd hh:mm' format. For example : '2015-07-11 15:00' ");
		}
		try{
			this.schedule = LocalDate.parse(sendDate, StringUtils.dateFormatter);
		}catch(DateTimeParseException d){
			throw new MessageException("Invalid Send Date Format: Make sure it's in 'yyyy-MM-dd hh:mm' format. For example : '2015-07-11 15:00' ");
		}
		return this;
	}
	
	/**
	 * Optional - Sets the date the system will stop trying to deliver the message if that recipient is not available to receive the message at that time. Default is 2 days from send date.
	 * @param expiryDate - in 'yyyy-MM-dd hh:mm' format
	 * @throws MessageException
	 */
	public Message setExpiryDate(String expiryDate) throws MessageException{
		if(null==expiryDate){
			throw new MessageException("Invalid Expiry Date Format: Make sure it's in 'yyyy-MM-dd hh:mm' format. For example : '2015-07-11 15:00' ");
		}
		try{
			this.expiry = LocalDate.parse(expiryDate, StringUtils.dateFormatter);
		}catch(DateTimeParseException d){
			throw new MessageException("Invalid Expiry Date Format: Make sure it's in 'yyyy-MM-dd hh:mm' format. For example : '2015-07-11 15:00' ");
		}
		return this;
	}

	
	public String getKey() {
		return this.key;
	}

	public String getSecret() {
		return this.secret;
	}

	/**
	 * Returns the value indicating whether a receipt is required or not. A receipt is sent to a URL of your choice, and this is set in your settings page on your account 
	 * @return receipt value. 0 or 1. Default = 1.
	 */
	public int getReceipt() {
		if(null == this.receipt){
			this.receipt = ReceiptRequest.RECEIPT_WANTED;
		}
		return this.receipt.getReceiptOption();
	}

	/**
	 * Returns the value of the encoding set. If none is set then the default 0 is returned (and subsequently used if a message is sent)
	 * @return encoding value between 0 and 8 inclusive.
	 */
	public int getEncoding() {
		if(null == this.encoding){
			this.encoding = SMSCharacterEncoding.SMSC_DEFAULT;
		}
		return this.encoding.getEncoding();
	}
	
	/**
	 * - Optional - Sets the character encoding to go along with the message. If message is unicode and no encoding is set SMSJuice API will detect the message and put the appropriate encoding, so no really need to use this. 
	 * @param encoding The encoding caluse to use. Recommendation is to use 0, 3 or 8 as values as they are
	 *  the most commonly used. The default is 0 if one doesn't select which one they want to use, and it usually suffices.
	 * @return Message object
	 * @throws MessageException triggered when one tries to instantiate the object with a value less than 0 or grater than 8 .
	 */
	public Message setEncoding(int encoding) throws MessageException{
		this.encoding = SMSCharacterEncoding.instanceFromValue(encoding);
		return this;
	}

	/**
	 * Returns the from address to be used in the message. This is the address that one sees on their phone as the sender of the message
	 * @return From-address
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * Returns the message content to be sent.
	 * @return Message content to be sent
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Returns the schedule date for when the message is to be sent. Can return null if not set, since it's optional
	 * @return Date for when message is to be sent
	 */
	public LocalDate getSchedule() {
		return schedule;
	}

	/**
	 * Returns the expiry date for when the message is to be deemed useless if not received by the recipient. Can return null if not set, since it's optional
	 * @return Date for when message expires
	 */
	public LocalDate getExpiry() {
		return expiry;
	}

	/**
	 * Returns the recipient list, or group list depending on what was put in.
	 * @return The recipient list, or group list
	 */
	public List<String> getTo() {
		return to;
	}
	
	/**
	 * Returns the indicator for what type of recipients are in the recipients list, whether it is a group or individual recipients
	 * @return Receipient type indicator
	 */
	public RecipientType getRecipientType(){
		return recipientType;
	}
	
	public int getTemplateId(){
		return this.templateId;
	}
	
	
    
}
