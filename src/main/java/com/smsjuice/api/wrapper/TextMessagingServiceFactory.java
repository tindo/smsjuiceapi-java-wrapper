package com.smsjuice.api.wrapper;

public class TextMessagingServiceFactory {

	private TextMessagingServiceFactory() {
	}
	
	public static TextMessagingService getLiveInstance(){
		return new TextMessagingService();
	}
	
	public static TextMessagingService getTestInstance(){
		return new TextMessagingService("http://localhost:8021");
	}
	
}
